# Download and compile Dlib 19.10

# TODO Check download integrity
# TODO Remove unnecesary build files to reduce folder size


# Load scripts
$SCRIPTS="D:\Proyectos\Powershell_scripts_build_dependencies"
. $SCRIPTS\DeleteFolderWithCheck.ps1
. $SCRIPTS\ShowStatusMessage.ps1


# Configure paths
$DLIB_DIR    = "D:\Librerias\Dlib_19.1"
$DLIB_TEMP   = "D:\Librerias\Dlib_temp"
$DLIB_SOURCE = "$DLIB_TEMP\source"
$OpenCV_DIR = "D:\Librerias\OpenCV_2.4.11"

# Download code
DeleteFolderWithCheck $DLIB_TEMP
New-Item -Type Directory -Path $DLIB_TEMP -Force
Set-Location $DLIB_TEMP
$src = "https://github.com/davisking/dlib/archive/v19.1.zip"
$dst = "$DLIB_TEMP/src.zip"
[Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12
$webClient = new-object System.Net.WebClient
$webClient.DownloadFile( "$src", "$dst" )


# Extract code
Expand-Archive "$DLIB_TEMP/src.zip" -DestinationPath $DLIB_TEMP
Rename-Item -Path "$DLIB_TEMP\dlib-19.1" -NewName $DLIB_SOURCE


function Build {
    Param($source, $build, $compiler)

    DeleteFolderWithCheck $build
    New-Item -Type Directory -Path $build -Force
    Set-Location $build
    
    # Generate project
    cmake -G "$compiler" `
        -DOpenCV_DIR:PATH=$OpenCV_DIR `
        -Wno-dev `
        $source
    
    # cmake-gui $build
    
    cmake --build . --config Debug
    ShowStatusMessage ($LastExitCode -eq 0) "Build OK" "Build error"

    cmake --build . --config Release
    ShowStatusMessage ($LastExitCode -eq 0) "Build OK" "Build error"  
}

# Build in 32 and 64 bits, debug and release
Build "$DLIB_SOURCE\examples" "$DLIB_TEMP\vs2013_x86" "Visual Studio 12 2013"
Build "$DLIB_SOURCE\examples" "$DLIB_TEMP\vs2013_x64" "Visual Studio 12 2013 Win64"


# Put everything together
Set-Location $DLIB_DIR
Copy-Item "$DLIB_TEMP\source\dlib" -destination "$DLIB_DIR\dlib" -Force -Recurse
Copy-Item "$DLIB_TEMP\vs2013_x86\dlib_build\Debug" -destination "$DLIB_DIR\lib\x86\vc12\" -Force -Recurse
Copy-Item "$DLIB_TEMP\vs2013_x86\dlib_build\Release" -destination "$DLIB_DIR\lib\x86\vc12\" -Force -Recurse
Copy-Item "$DLIB_TEMP\vs2013_x86\Release" -destination "$DLIB_DIR\examples\x86\vc12\" -Force -Recurse
Copy-Item "$DLIB_TEMP\vs2013_x64\dlib_build\Debug" -destination "$DLIB_DIR\lib\x64\vc12\" -Force -Recurse
Copy-Item "$DLIB_TEMP\vs2013_x64\dlib_build\Release" -destination "$DLIB_DIR\lib\x64\vc12\" -Force -Recurse
Copy-Item "$DLIB_TEMP\vs2013_x64\Release" -destination "$DLIB_DIR\examples\x64\vc12\" -Force -Recurse


# Get models
New-Item -Type Directory -Path "$DLIB_DIR\models" -Force
$webClient.DownloadFile( "http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2", "$DLIB_DIR\models\shape_predictor_68_face_landmarks.dat.bz2" )


# Remove temp compilations
Set-Location $DLIB_DIR
DeleteFolderWithCheck $DLIB_TEMP


Wait-Event
