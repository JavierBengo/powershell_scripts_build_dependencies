
$DesktopPath = [Environment]::GetFolderPath("Desktop")
$ProgFilesX86 = [Environment]::GetFolderPath("ProgramFilesX86")
$ProgFilesX64 = [Environment]::GetFolderPath("ProgramFiles")
$ProgFilesPortable = "C:\Program Files Portable"



# RapidEE
Invoke-WebRequest -Uri "https://www.rapidee.com/download/RapidEE_setup.exe" -OutFile "$DesktopPath\RapidEE_setup.exe"
& "$DesktopPath\RapidEE_setup.exe" /silent
Start-Sleep -s 10
Remove-Item "$DesktopPath\RapidEE_setup.exe"

# Glary
Invoke-WebRequest -Uri "http://download.glarysoft.com/gu5setup.exe" -OutFile "$DesktopPath\gu5setup.exe"
& "$DesktopPath\gu5setup.exe"
$host.UI.RawUI.ReadKey()
Remove-Item "$DesktopPath\gu5setup.exe"

# VS2013.5
Invoke-WebRequest -Uri "https://download.microsoft.com/download/A/F/9/AF95E6F8-2E6E-49D0-A48A-8E918D7FD768/VS2013.5.exe" -OutFile "$DesktopPath\VS2013.5.exe"
& "$DesktopPath\VS2013.5.exe" /Passive /NoRestart
$host.UI.RawUI.ReadKey()
Remove-Item "$DesktopPath\VS2013.5.exe"
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "VS2013" -Value "$ProgFilesX86\Microsoft Visual Studio 12.0\Common7\IDE\devenv.exe"

# Dependency walker x86
Invoke-WebRequest -Uri "http://www.dependencywalker.com/depends22_x86.zip" -OutFile "$DesktopPath\depends22_x86.zip"
Expand-Archive "$DesktopPath\depends22_x86.zip" -DestinationPath "$ProgFilesPortable\DependencyWalker_x86"
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "Dependency Walker x86" -Value "$ProgFilesPortable\DependencyWalker_x86\depends.exe"
Remove-Item "$DesktopPath\depends22_x86.zip"

# Dependency walker x64
Invoke-WebRequest -Uri "http://www.dependencywalker.com/depends22_x64.zip" -OutFile "$DesktopPath\depends22_x64.zip"
Expand-Archive "$DesktopPath\depends22_x64.zip" -DestinationPath "$ProgFilesPortable\DependencyWalker_x64"
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "Dependency Walker x64" -Value "$ProgFilesPortable\DependencyWalker_x64\depends.exe"
Remove-Item "$DesktopPath\depends22_x64.zip"

# API monitor
Invoke-WebRequest -Uri "www.rohitab.com/download/api-monitor-v2r13-x86-x64.zip" -OutFile "$DesktopPath\api-monitor.zip"
Expand-Archive "$DesktopPath\api-monitor.zip" -DestinationPath "$ProgFilesPortable\"
$temp = Get-ChildItem "$ProgFilesPortable\API Monitor*" -Name
Rename-Item -Path "$ProgFilesPortable\$temp" -NewName "$ProgFilesPortable\API Monitor"
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "API monitor x86" -Value "$ProgFilesPortable\API Monitor\apimonitor-x86.exe"
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "API monitor x64" -Value "$ProgFilesPortable\API Monitor\apimonitor-x64.exe"
Remove-Item "$DesktopPath\api-monitor.zip"

# Sysinternals
Invoke-WebRequest -Uri "https://download.sysinternals.com/files/SysinternalsSuite.zip" -OutFile "$DesktopPath\SysinternalsSuite.zip"
Expand-Archive "$DesktopPath\SysinternalsSuite.zip" -DestinationPath "$ProgFilesPortable\Sysinternals"
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";$ProgFilesPortable\Sysinternals", [EnvironmentVariableTarget]::Machine)
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "Sysinternals" -Value "$ProgFilesPortable\Sysinternals"
Remove-Item "$DesktopPath\SysinternalsSuite.zip"


# HexEdit
# Invoke-WebRequest -Uri "https://www.mediafire.com/?b8wb4ainrm20vm3" -OutFile "$DesktopPath\HexEdit.zip"
# Expand-Archive "$DesktopPath\SysinternalsSuite.zip" -DestinationPath "$ProgFilesPortable\Sysinternals"
# [Environment]::SetEnvironmentVariable("Path", $env:Path + ";$ProgFilesPortable\Sysinternals", [EnvironmentVariableTarget]::Machine)
# New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "Sysinternals" -Value "$ProgFilesPortable\Sysinternals"
# Remove-Item "$DesktopPath\SysinternalsSuite.zip"










