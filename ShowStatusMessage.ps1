# Show a success message in green or a failure message in red based on a condition

function ShowStatusMessage {
    Param($condition, $success, $failure)

if ($condition) {
    Write-Host -BackgroundColor Green -ForegroundColor Black $success 
}
else {
    Write-Host -BackgroundColor Red -ForegroundColor Black $failure 
    Pause
}

}