# Download and compile OpenCV 2.4.11

# TODO Check download integrity
# TODO Remove unnecesary build files to reduce folder size


# Load scripts
$SCRIPTS="D:\Proyectos\Powershell_scripts_build_dependencies"
. $SCRIPTS\DeleteFolderWithCheck.ps1
. $SCRIPTS\ShowStatusMessage.ps1


# Configure paths
$OPENCV_DIR    = "D:\Librerias\OpenCV_2.4.11"
$OPENCV_TEMP   = "D:\Librerias\OpenCV_2.4.11_temp"
$OPENCV_SOURCE = "$OPENCV_TEMP\source"


# Download code
DeleteFolderWithCheck $OPENCV_TEMP
New-Item -Type Directory -Path $OPENCV_TEMP -Force
Set-Location $OPENCV_TEMP
$src = "http://github.com/opencv/opencv/archive/2.4.11.zip"
$dst = "$OPENCV_TEMP/opencv-2.4.11.zip"
[Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12
$webClient = new-object System.Net.WebClient
$webClient.DownloadFile( "$src", "$dst" )


# Extract code
Expand-Archive "$OPENCV_TEMP/opencv-2.4.11.zip" -DestinationPath $OPENCV_TEMP
Rename-Item -Path "$OPENCV_TEMP\opencv-2.4.11" -NewName $OPENCV_SOURCE


# Apply patch
Copy-Item "$SCRIPTS\Patches\OpenCV_2.4.11_cap_dshow.cpp" -destination "$OPENCV_SOURCE\modules\highgui\src\cap_dshow.cpp" -Force


function Build {
    Param($source, $build, $compiler, $shared)

    DeleteFolderWithCheck $build
    New-Item -Type Directory -Path $build -Force
    Set-Location $build
    
    # Generate project
    cmake -G "$compiler" `
        -DBUILD_DOCS="OFF" `
        -DBUILD_PERF_TESTS="OFF" `
        -DBUILD_TESTS="OFF" `
        -DBUILD_SHARED_LIBS="$shared" `
        -DBUILD_WITH_STATIC_CRT="$shared" `
        -Wno-dev `
        $source
    
    # cmake-gui $build
    
    cmake --build . --config Debug
    ShowStatusMessage ($LastExitCode -eq 0) "Build OK" "Build error"

    cmake --build . --config Release
    ShowStatusMessage ($LastExitCode -eq 0) "Build OK" "Build error"  
    
    cmake --build . --target install --config Debug
    ShowStatusMessage ($LastExitCode -eq 0) "Build OK" "Build error"

    cmake --build . --target install --config Release
    ShowStatusMessage ($LastExitCode -eq 0) "Build OK" "Build error"
}

# Build shared and static libs, in 32 and 64 bits, debug and release
Build "$OPENCV_SOURCE" "$OPENCV_TEMP\vs2013_x86_shared" "Visual Studio 12 2013" "ON"
Build "$OPENCV_SOURCE" "$OPENCV_TEMP\vs2013_x86_static" "Visual Studio 12 2013" "OFF"
Build "$OPENCV_SOURCE" "$OPENCV_TEMP\vs2013_x64_shared" "Visual Studio 12 2013 Win64" "ON"
Build "$OPENCV_SOURCE" "$OPENCV_TEMP\vs2013_x64_static" "Visual Studio 12 2013 Win64" "OFF"

# Put everything together
Copy-Item "$OPENCV_TEMP\vs2013_x86_shared\install" -destination "$OPENCV_DIR\" -Force -Recurse
Copy-Item "$OPENCV_TEMP\vs2013_x86_static\install\x86\vc12\bin" -destination "$OPENCV_DIR\x86\vc12\staticbin" -Force -Recurse
Copy-Item "$OPENCV_TEMP\vs2013_x86_static\install\x86\vc12\staticlib" -destination "$OPENCV_DIR\x86\vc12\staticlib" -Force -Recurse
Copy-Item "$OPENCV_TEMP\vs2013_x64_shared\install\x64" -destination "$OPENCV_DIR\x64" -Force -Recurse
Copy-Item "$OPENCV_TEMP\vs2013_x64_static\install\x64\vc12\bin" -destination "$OPENCV_DIR\x64\vc12\staticbin" -Force -Recurse
Copy-Item "$OPENCV_TEMP\vs2013_x64_static\install\x64\vc12\staticlib" -destination "$OPENCV_DIR\x64\vc12\staticlib" -Force -Recurse

# Remove temp compilations
Set-Location $OPENCV_DIR
DeleteFolderWithCheck $OPENCV_TEMP


Wait-Event
