# Remove-Item sometimes doesn't work well, like when having several subfolder levels or the folder is in use.
# This function won't end until the folder is deleted.

function DeleteFolderWithCheck {
    Param($folder)

    Write-Host -BackgroundColor Cyan -ForegroundColor Black "Delete Folder $folder"

    While (Test-Path $folder)
    {
        Write-Output "   Deleting..." 

        Remove-Item $folder -Force -Recurse
        Start-Sleep -m 10
    }    

    Write-Host -BackgroundColor Green -ForegroundColor Black "$folder was removed" 
}