# Installs chocolatey and all the packages I need
# I prefer to have all the SW in ProgFiles, not in ProgData, so some programs will be removed

# TODO check outputs 

$DesktopPath = [Environment]::GetFolderPath("Desktop")
$ProgFilesX86 = [Environment]::GetFolderPath("ProgramFilesX86")
$ProgFilesX64 = [Environment]::GetFolderPath("ProgramFiles")
$ProgFilesPortable = "C:\Program Files Portable"


Set-ExecutionPolicy Bypass -Scope Process -Force
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

choco feature enable -n allowGlobalConfirmation

# PC control
choco install mousewithoutborders
# choco install skype 
choco install teamviewer
choco install anydesk
& "C:\ProgramData\chocolatey\lib\anydesk\tools\anydesk.exe" --install "$ProgFilesX86\AnyDesk" --start-with-win --create-shortcuts --create-desktop-icon
choco uninstall anydesk


# web
choco install firefox
choco install googlechrome
# choco install flashplayerplugin
# choco install flashplayeractivex
# choco install jre8
# choco install javaruntime 


# programming
choco install visualstudiocode
choco install visualstudio2013ultimate
# choco install notepadplusplus.install
choco install git
choco install sourcetree
choco install cmake.install
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";$ProgFilesX64\CMake\bin", [EnvironmentVariableTarget]::Machine)
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "CMake" -Value "$ProgFilesX64\CMake\bin\cmake-gui.exe"
choco install nsis.install
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";$ProgFilesX86\NSIS\Bin", [EnvironmentVariableTarget]::Machine)
choco install winmerge
# choco install python2 
# choco install strawberryperl 
# choco install indentguides 


# multimedia
choco install foobar2000
choco install mpc-hc
# choco install freevideoeditor 
# choco install vlc 
# choco install k-litecodecpackfull
# choco install k-litecodecpackmega
choco install qbittorrent 


# other
choco install 7zip.install
# choco install avastfreeantivirus 
# choco install produkey.install 


# Security
choco install explorersuite
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "CFF Explorer" -Value "$ProgFilesX64\NTCore\Explorer Suite\CFF Explorer.exe"

choco install wireshark
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "Wireshark" -Value "$ProgFilesX64\Wireshark\Wireshark.exe"

choco install nmap

choco install dotpeek.portable
New-Item -path "$ProgFilesPortable\DotPeek" -type directory
Copy-Item "C:\ProgramData\Chocolatey\lib\dotpeek.portable\tools\dotPeek*" -Destination "$ProgFilesPortable\DotPeek" -Force
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "dotPeek" -Value "$ProgFilesPortable\DotPeek\dotPeek.exe"
choco uninstall dotpeek.portable

choco install x64dbg.portable
Copy-Item "C:\ProgramData\Chocolatey\lib\x64dbg.portable\tools\release" -Destination "$ProgFilesPortable\x64dbg" -Force -Recurse
New-Item -ItemType SymbolicLink -Path "$DesktopPath" -Name "x64dbg" -Value "$ProgFilesPortable\x64dbg\x96dbg.exe"
choco uninstall x64dbg.portable


