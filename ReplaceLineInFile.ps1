# Replace a whole line in a text file when it contains a given fragment.
# 
# Example:
# Given a text file named config.h containing the following text:
# #define VERSION_STR "2.4.0"
# We want to replace it with:
# #define VERSION_STR "2.4.1"
# So we call
# ReplaceLineInFile "./config.h" "#define VERSION_STR" "#define VERSION_STR ""2.4.1"""

function ReplaceLineInFile {
    Param($file, $fragment, $replacement)

    Write-Host -BackgroundColor Cyan -ForegroundColor Black "$file" 

    if (!(Test-Path $file)) {
        Write-Host -BackgroundColor Red -ForegroundColor Black "File does not exist" 
        Return
    }

    $CMSReport = Get-content $file
    $CMSReport | ForEach-Object {
        if ($_.Contains($fragment)) {
            $replacement
            Write-Host -BackgroundColor Green -ForegroundColor Black "$_ => $replacement" 
        } else {
            $_
        }
    } |
    Set-Content $file

    Write-Output " " 
}